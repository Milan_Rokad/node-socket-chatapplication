let express = require("express");
let app = express();
let http = require("http").createServer(app);
let io = require("socket.io")(http);

app.set("view engine", "ejs");
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

users_list = [];
rooms_list = ["Room_1", "Room_2"];
// online_users_list = [];

app.get("/", function(req, res) {
    res.render("index");
    console.log(users_list);
});

io.on("connection", socket => {
    socket.on("username", username => {
        user = username.trim();
        if (
            !users_list.some(elem => socket.id === elem.id) &&
            !users_list.some(elem => user === elem.username) &&
            user &&
            user != ""
        ) {
            users_list.push({
                id: socket.id,
                username: user
            });

            socket.join(rooms_list);
            io.emit("usersList", users_list);
            io.emit("roomslist", { rooms_list });
        }
        console.log("USERS LIST: " + users_list);
        console.log("ROOMS LIST: " + rooms_list);
    });

    socket.on("sendMsg", msg => {
        console.log("MSG RECEIVED: " + JSON.stringify(msg));
        socket.broadcast.to(msg.to).emit("getMsg", {msg: msg.msg, from: socket.id});
    });

    socket.on("isWriting", () => {
        socket.broadcast.to(msg.to).emit("yesWriting");
    });

    socket.on("disconnect", () => {
        console.log("dicconect? " + socket.id);
        users_list = users_list.filter(item => item.id !== socket.id);
        io.emit("usersList", users_list);
    });
});

http.listen(8080, "192.168.2.8", function() {
    console.log("listening on localhost: 8080");
});
